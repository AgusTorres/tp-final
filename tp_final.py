#Tp final ICB
#%%
import csv
import numpy as np
from datetime import datetime as time
from datetime import timedelta
import folium
import matplotlib.pyplot as plt

#%%

#Definiciones de funciones

def convertir_fecha(dia, hora):
    """
    Recibe dos cadenas de caracteres con un formato determinado y los transforma en 
    tipo de dato fecha-hora de python.
    Si el string hora corresponde a un entero mayor a 23 se incrementa un dia por
    cada 24 horas adicionales.
    Formato fecha que recibe: (fila[0]+fila[1]) 10MAY2019:00:00:00 22
    Devuelve: datetime.datetime(2019, 5, 10, 22, 0)
    """
    fechita = time.strptime(dia, "%d%b%Y:%H:%M:%S")
    int_hora = int(hora)
    if int_hora < 24:
        fechita = fechita + timedelta(hours= int_hora)
    else:
        dias = int_hora // 24
        horas = int_hora % 24
        fechita = fechita + timedelta(days = dias) + timedelta(hours=horas)
    return fechita

####Test convertir_fecha
#d = "10MAY2019:00:00:00"
#h = "50"
#convertir_fecha(d, h) 
# %%
def upSortFechas(f, e):
    """
    recibe una lista (f) y una lista de listas (e)
    modifica sus parametros de entrada: ordena la lista f
    de forma creciente y realiza las mismas permutaciones en 
    las sublistas que componen la lista e
    """
    actual = len(f) - 1                     #ultimo elemento de f
    m = 0
    while actual > 0:
        m = maxPos(f, 0, actual)            #busco la posicion del valor maximo de f entre 0 y actual
        f[m], f[actual] = f[actual], f[m]   #permuto
        for lista in e:
            lista[m], lista[actual] = lista[actual], lista[m]   #misma permutacion en las sublistas
        actual -= 1

####Test upSortFechas
#Para correr este test es necesario tener cargados los datos de fechas y de NO2
#l_test = []
#e_test = [[],[]]
#for i in range(20):
#    l_test.append(fecha[i])
#    e_test[0].append(NO2_estaciones[0][i])
#    e_test[1].append(NO2_estaciones[1][i])
#upSortFechas(l_test, e_test)

def maxPos(a, d, h):
    """
    devuelve el indice correspondiente al valor maximo
    qe se encuentre entre las posiciones d y h (inclusive)
    """
    res = d
    maxim = a[d]
    while d <= h:
        if a[d] > maxim:
            maxim = a[d]
            res = d
        d += 1
    return res

def primer_lunes(fecha):
    '''
    Recibe una lista de fechas ordenada y devuelve el indice de la fecha correspondiente al primer lunes.
    La lista que recibe debe ser no vacia, ordenada y contener al menos un lunes
    '''
    encontre_lunes = False
    res = 0
    i = 0
    while i < len(fecha) and not encontre_lunes:
        if time.weekday(fecha[i]) == 0:    #.weekday() devuelve 0 si recibe un lunes
            encontre_lunes = True
            res = i
        else:
            i += 1
    return res
####Test primer lunes
#para correr el test es necesario tener cargados los datos de fechas_ordenadas
#primer_lunes(fechas_ordenadas)
#time.weekday(fechas_ordenadas[34])

def extraer_semana(fecha):
    """
    Recibe una lista de fechas ordenadas y devuelve una lista de listas:
    cada lista corresponde a los indices de fechas correspondientes a una
    misma semana.
    De estar incompleta la ultima semana, esas fechas quedaran afuera, sus
    indices no serán incluidos dentro de ninguna sublista.
    """
    indices_semanas = []                        
    indice_lunes = primer_lunes(fecha)
    lunes = fecha[indice_lunes]
    i = indice_lunes
    semana = []
    while i < len(fecha):
        if fecha[i] < (lunes + timedelta(days = 7)):
            semana.append(i)
        else:
            indices_semanas.append(semana)      #agrego la lista semana a la lista indices_semanas cuando llego al siguiente lunes
            semana = [i]                         #vacio la lista, y agrego el lunes que encontre, para entrar de nuevo en el ciclo
            lunes = lunes + timedelta(days = 7) #actualizo el lunes
        i += 1
    return indices_semanas

#El checkeo del funcionamiento de esta funcion se realizo 
#al utilizarla para obtener los indices en el programa
#principal.

def obtener_promedios(NO2_estaciones_ordenadas, fechas_ordenadas,indices_semanas):
    """
    Recibe una lista de listas con datos de NO2 correspondientes a 4 estaciones, 
    una lista de fechas ordenadas (de fechas anteriores
    a fechas posteriores), y una lista de listas, donde cada lista interna corresponde a
    una misma semana y contiene indices de la lista de fechas ordenadas.
    Devuelve cinco listas: una lista con la fecha de inicio de cada semana, y cuatro
    listas con los promedios semanales correspondientes a cada estacion.
    """     
    fechas_inicio_semana = []           # hasta Nov 2021
    for i  in range(len(indices_semanas)): 
        indice = indices_semanas[i][0]  #Indices_semanas es una lista de listas, el elemento 0 de c/lista es el 1er indice de c/semana
        fechas_inicio_semana.append(fechas_ordenadas[indice])
    
    promedios = [[],[],[],[]]
    k = 0
    while k < len(NO2_estaciones_ordenadas):  # recorremos las estaciones de NO2
        for i in range(len(indices_semanas)): # recorremos la lista de semanas 
            suma = 0
            div = 0
            for indice in indices_semanas[i]: # recorremos cada indice de una misma semana
                if NO2_estaciones_ordenadas[k][indice] != 'NaN': # si el dato para ese indice es distinto a NaN lo suma y agrega un 1 al contador div 
                    suma+=float(NO2_estaciones_ordenadas[k][indice]) 
                    div += 1 # contamos datos que no son NaN
            if div != 0: # si el div es distinto de cero calcula el promedio con la longitud de ese div (hay al menos un valor distinto a NaN)
                promedio = suma / div
            else:
                promedio = 0 # caso contrario el promedio es cero
            promedios[k].append(promedio) # se agrega a cada una de las estaciones correspondientes.
        k +=1

    return fechas_inicio_semana,promedios[0],promedios[1],promedios[2],promedios[3]

####Test obtener promedios
#NO2 = [[2,4,2,4,5,10,5,10], [5,10,5,10,2,4,2,4]]
#fechas_test = [time(2009, 10, 5, 0, 0), time(2009, 10, 5, 1, 0),\
#    time(2009, 10, 5, 2, 0), time(2009, 10, 5, 3, 0),\
#    time(2009, 10, 12, 0, 0), time(2009, 10, 12, 1, 0), \
#    time(2009, 10, 12, 2, 0), time(2009, 10, 12, 3, 0)]
#indices_test = [[0,1,2,3],[4,5,6,7]]
#prom_test = obtener_promedios(NO2, fechas_test, indices_test)
#fechitas = prom_test[0]
#e1 = prom_test[1]
#e2 = prom_test[2]

def extraer_año(fecha): 
    '''
    Consideramos el primer año a partir del primer lunes que tenemos datos : 2009-10-05 00:00:00
    Por lo tanto el siguiente año corresponde a :2010-10-05 00:00:00 . Asi sucesivamente . 
    El año 2021 no se esta considerando porque corresponderia al 2022-10-05 00:00:00 , que se va del rango de datos.
    '''
    indices_años = []
    i = primer_lunes(fecha) # indice del primer dia de un año, uso el primer lunes de las mediciones de semanas
    primer_año = fecha[i]   #accedo a ese primer dia
    años = []
    
    while i < len(fecha):   #recorro la lista de fechas ordenadas para saber cuales pertenecen a un mismo año y cuales no
        if fecha[i] < (primer_año + timedelta(days = 365)): 
            años.append(i)  #si estoy dentro del mismo año agrego la fecha la lista
        else:
            indices_años.append(años)   # si estoy en el proximo año ,agrego la lista años a indice_años
            años= [i]                   # reseteo la lista años , agregando el indice del lunes correspondiente al año proximo para entrar al ciclo nuevamente.                   
            primer_año = primer_año + timedelta(days = 365) #actualizo el año 
        i += 1

    return indices_años


def promedios_anuales(NO2_estaciones_ordenadas, fechas_ordenadas,indices_años):  
    '''
    Recibe una lista de listas, donde cada lista interna corresponde a una misma estacion meteorologica y contiene datos de NO2.
    Recibe la lista de fechas ordenadas y la lista de listas de indices_años
    Calcula el promedio anual por estacion de N02 ambiental, recolecta los datos NaN en cada una de ellas
    para poder estimar luego cuales de las estaciones permanecio mas tiempo sin funcionar.
    '''
    fechas_inicio_año = []
    for i  in range(len(indices_años)): 
        indice = indices_años[i][0]  #Indices_años es una lista de listas, el elemento 0 de cada sublista es el primer indice de cada año
        fechas_inicio_año.append(fechas_ordenadas[indice])

    k=0
    promedios = [[],[],[],[]]
    NaN_encontrados = [[],[],[],[]]
    while k < len(NO2_estaciones_ordenadas):  # recorremos cada Estacion meteorologica
        for i in range(len(indices_años)):    # recorremos la lista de indices de años 
            suma = 0
            div = 0
            cuenta_NaN = 0
            for indice in indices_años[i]:   # recorremos los indices de cada dato de un mismo año
                if NO2_estaciones_ordenadas[k][indice] != 'NaN': # si es un numero lo suma para calcular al promedio
                    suma+=float(NO2_estaciones_ordenadas[k][indice]) 
                    div += 1 # contador de numeros
                else:
                    cuenta_NaN += 1 # contador de elementos NaN
            if div != 0:  # porque sino estamos frente a una indefinición
                promedio = suma / div 
            else:
                promedio = 0 
            promedios[k].append(promedio)
            NaN_encontrados[k].append(cuenta_NaN) # cuento los NaN por estacion/ año
        k +=1 # avanzo con la siguiente estacion

    # genero lista del total de NaN encontrados para cada estacion para todos los años considerados
    total_NaN_porEstacion = []
    for datos in NaN_encontrados:
        total_NaN_porEstacion.append(sum(datos))

    return fechas_inicio_año ,promedios[0],promedios[1],promedios[2],promedios[3],total_NaN_porEstacion

def promedios_anuales_generales(NO2_estaciones_ordenadas, indices_años):
    """
    Recibe una lista de listas, donde cada lista interna corresponde a 
    una misma estacion meteorologica y contiene datos de NO2. Estos
    datos tienen que ser "NaN" o poder transformarse en float.
    Recibe tambien una lista de listas, donde cada lista interna 
    corresponde a un mismo año y contiene indices (num enteros) de la lista de
    datos de NO2.
    Calcula el promedio anual de todos los datos de NO2. Devuelve esos
    promedios.
    Al calcular los promedios solo se tienen en cuenta datos que no
    corresponden a "NaN".
    """
    promedios_generales = []
    for i in range(len(indices_años)):  #recorro indices_años, que contiene sublistas
        suma = 0
        div = 0
        for indice in indices_años[i]:  #accedo a cada elemento de cada sublista
            for k in range(len(NO2_estaciones_ordenadas)):  #accedo a datos de NO2 de c/estacion
                if NO2_estaciones_ordenadas[k][indice] != "NaN":    #solo sumo si hay un numero
                    suma += float(NO2_estaciones_ordenadas[k][indice])
                    div += 1
        if div != 0:
            prom_anual = suma / div #calculo el promedio
        else:
            prom_anual = 0
        promedios_generales.append(prom_anual)
    return promedios_generales

#test prom_anuales_generales
#NO2=[[2,3,4,10,10,10,0,0,0],[2,3,4,10,10,10,0,0,0]]
#indices = [[0,1,2],[3,4,5],[6,7,8]]
#prueba = promedios_anuales_generales(NO2,indices)
#NO2=[['2','3','4','10','10','10','NaN','NaN','NaN'],['2','3','4','10','10','10','NaN','NaN','NaN']]
#prueba = promedios_anuales_generales(NO2,indices)

def generador_radio(promedio_semanal, year, years, prom_anuales_generales):
    """
    Funcion que recibe un numero que representa el promedio_semanal, un
    numero que corresponde al año de dicha semana, una lista de enteros
    que representan años y una lista de promedios anuales.
    El año deberá estar presente en la lista de años o ser 2021.
    Devuelve un radio (un real) proporcional al cociente entre el
    promedio semanal y el promedio anual correspondiente.
    En el caso de datos posteriores al 2 de octubre del 2021
    (ultima fecha incluida en el año que llamamos 2020), el radio 
    sera el promedio semanal dividido por el promedio anual del 
    2020.
    """
    i = 0
    encontre_anio = False
    while i < len(years) and not encontre_anio:
        if years[i] == year:
             encontre_anio = True
        elif year == 2021:
            i = 11  #uso el promedio de 2020 para los datos de 2021
            encontre_anio = True
        else:
            i += 1
    factor = promedio_semanal / prom_anuales_generales[i]
    radio = 20 * factor
    return radio

#test = generador_radio(27.0766955187904, 2009, years, prom_anuales_generales)

#%%
######COMIENZA EL PROGRAMA PRINCIPAL
nombre_estaciones_NO2 = ["NO2_centenario","NO2_cordoba","NO2_boca","NO2 palermo"]

NO2_estaciones =  [[],[],[],[]]
indices_NO2 = [3, 6, 9, 12]
fecha = []

with open("calidad-aire.csv", "rt") as calidad_aire:
    filas = csv.reader(calidad_aire)    #leo el archivo .csv
    encabezado = next(filas)            #la primer fila corresponde al encabezado , la saltea con el comando next
    for fila in filas:
        fecha.append(convertir_fecha(fila[0], fila[1]))      
        for e, i in enumerate(indices_NO2): #e toma valores entre 0 y 3, los indices de NO2_estaciones
            try:                            #i corresponde a los elementos en indices_NO2
                NO2_estaciones[e].append(float(fila[i]))
            except:
                if len(fila[i]) > 0 and fila[i][0] == '<': # si tiene el elemento tipo <
                    lista = list(fila[i]) # lo transformo a una lista para poder trabajarlo
                    lista.pop(0) # elimino ese simbolo
                    valor = "".join(lista) # lo vuelvo a pasar a string
                    valor = float(valor) # lo convierto en numero
                    NO2_estaciones[e].append(valor) # lo agrego a la lista
                else:
                    NO2_estaciones[e].append('NaN') # apendea un NaN en las listas correspondientes
                
#print(len(NO2_estaciones[0]))
#print(len(NO2_estaciones[1]))
#print(len(NO2_estaciones[2]))
#print(len(NO2_estaciones[3]))

#%%
#Ordeno fechas y listas
upSortFechas(fecha, NO2_estaciones)
#Este bloque de codigo demora en correr, vamos a guardar las listas que obtenemos
#para no tener que ejecutarlo varias veces.
#Transformo las listas en arrays de numpy
fecha_arr = np.array(fecha)
NO2_estaciones_arr = np.array(NO2_estaciones)
#las guardo
#ruta_fecha = "/home/ailu/ICB/tp-final/fecha.npy"
#ruta_estaciones = "/home/ailu/ICB/tp-final/NO2_estaciones.npy"
ruta_fecha = f"/Users/agust/Desktop/I.Computacion/ejercicios_icb/Nueva carpeta/tp-final/fecha.npy"
ruta_estaciones = f"/Users/agust/Desktop/I.Computacion/ejercicios_icb/Nueva carpeta/tp-final/NO2_estaciones.npy"

np.save(ruta_fecha, fecha_arr)
np.save(ruta_estaciones, NO2_estaciones_arr)

#%%
#Trabajamos ahora con las listas ordenadas
fechas_ordenadas = np.load("fecha.npy", allow_pickle=True)    #cargo el archivo .np
fechas_ordenadas = list(fechas_ordenadas)                           #lo transformo en tipo lista
NO2_estaciones_ordenadas = np.load("NO2_estaciones.npy")
NO2_estaciones_ordenadas = list(NO2_estaciones_ordenadas)
#%%
#Obtencion de las listas necesarias para realizar los graficos y mapas

indices_semanas = extraer_semana(fechas_ordenadas)
#len(indices_semanas) #633

#Checkeo que no quede ningun indice afuera
#indices_semanas[0]
#indices_semanas[1]
#indices_semanas[2]
#indices_semanas[631]
#indices_semanas[632]

promedios = obtener_promedios(NO2_estaciones_ordenadas, fechas_ordenadas,indices_semanas)

inicio_semana = promedios[0]
promedio_semanal_centenario = promedios[1]
promedio_semanal_cordoba = promedios[2]
promedio_semanal_la_boca = promedios[3]
promedio_semanal_palermo = promedios[4]

#Checkeo que fechas inicio_semana coincidan con las fechas de fechas_ordenadas
#print(inicio_semana[0])
#print(fechas_ordenadas[indices_semanas[0][0]])
#print(inicio_semana[34])
#print(fechas_ordenadas[indices_semanas[34][0]])
#print(fechas_ordenadas[indices_semanas[632][0]])

# calculo promedios anuales  para hace el grafico de la variacion de N02 en funcion del tiempo

indices_años = extraer_año(fechas_ordenadas)
#print(len(indices_años)) #12

#Verificacion de que todos los indices  esten incluidos
#print(indices_años[0][8104])
#print(indices_años[1][0])
#print(indices_años[1][8776])

promedio_anual = promedios_anuales(NO2_estaciones_ordenadas, fechas_ordenadas,indices_años)
fechas_inicio_año= promedio_anual[0]
promedio_anual_centenario = promedio_anual[1]
promedio_anual_Cordoba= promedio_anual[2]
promedio_anual_LaBoca = promedio_anual[3]
promedio_anual_Palermo = promedio_anual[4] 
total_NaN_porEstacion = promedio_anual[5]

#Verifico que coincidan los indices
#print(fechas_inicio_año[10])
#print(fechas_ordenadas[indices_años[10][0]])

prom_anuales_generales = promedios_anuales_generales(NO2_estaciones_ordenadas, indices_años)
years = [2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020]

#Listas para mapas

nombres_estaciones_amb = ["La Boca", "Parque Centenario", "Cordoba", "Palermo"]
longitudes = []
latitudes = []
direccion = []
info = []

with open("estaciones-ambientales.csv", "rt", errors="ignore") as estaciones_amb:
    filas = csv.reader(estaciones_amb, delimiter = ';')    #leo el archivo .csv
    encabezado = next(filas)            #la primer fila corresponde al encabezado , la saltea con el comando next
    for fila in filas:
        longitudes.append(float(fila[0].replace(',', '.'))) #no puedo transformar numeros con , a float
        latitudes.append(float(fila[1].replace(',', '.'))) 
        direccion.append(fila[3])
        info.append(fila[5])

#Ubicacion promedio
longitud_media = sum(longitudes)/len(longitudes)
latitud_media = sum(latitudes)/len(latitudes)
# %%
#######GRAFICOS

#Grafico 1) una opcion con los promedios anuales

fig = plt.figure(figsize=(9,7))
plt.title("Variación del promedio anual de N02 para cuatro estaciones meteorologicas en CABA. \n" + " Rango anual 2009 - 2020.", bbox={'facecolor':'#FFB6C1', 'pad':3}, position=(0.5, 0.5),size = 13)
plt.plot(fechas_inicio_año, promedio_anual_centenario, label= "Parque Centenario")
plt.plot(fechas_inicio_año, promedio_anual_Cordoba, label= "Av.Cordoba")
plt.plot(fechas_inicio_año, promedio_anual_LaBoca, label= "La Boca")
plt.plot(fechas_inicio_año, promedio_anual_Palermo, label= "Palermo")
plt.xlabel("Años", size=12)
plt.ylabel("Promedios anuales de NO2" , size = 12)
plt.legend(loc='best',fontsize= 13)
plt.savefig("variacionN02vsTiempo_4estaciones.pdf")
plt.show()

#%%
# Grafico 2) zoom sobre las estaciones de Parque Centenario, Av. Cordoba y La Boca que tienen datos de NO2 ambiental 

fig = plt.figure(figsize=(9,7))
plt.title("Variación del promedio anual de N02 para tres estaciones meteorologicas en CABA. \n" + " Rango anual 2009 - 2020.", bbox={'facecolor':'#FFB6C1', 'pad':3}, position=(0.5, 0.5),size = 13)
plt.plot(fechas_inicio_año, promedio_anual_centenario, label= "Parque Centenario")
plt.plot(fechas_inicio_año, promedio_anual_Cordoba, label= "Av.Cordoba")
plt.plot(fechas_inicio_año, promedio_anual_LaBoca, label= "La Boca")
plt.xlabel("Años", size=12)
plt.ylabel("Promedios anuales de NO2" , size = 12)
plt.yticks(np.arange(min(promedio_anual_LaBoca), max(promedio_anual_Cordoba)+ 0.11, 1))
plt.legend(loc='best',fontsize= 13)
plt.savefig("variacionN02vsTiempo_3estaciones.pdf")
plt.show()

# ¿cual es la estacion que tuvo mas tiempo el sensor sin funcionar? 
#pie chart - proporcion de NaN sobre el total de NaN mostrado en porcentaje para cada estacion
estaciones = 'Parque Centenario', 'Av. Cordoba','La Boca' , 'Palermo'
explode = (0, 0, 0, 0.1)
plt.figure(figsize=(9, 5))
plt.pie(total_NaN_porEstacion, explode=explode, labels=estaciones,autopct='%1.1f%%', shadow=True, startangle=140, textprops={'fontsize': 14}, radius = 1.3)
plt.title("Porcentaje de datos ausentes (NaN) para cuatro estaciones meteorologicas en CABA. \n" + " (años 2009 - 2020 )", bbox={'facecolor':'0.8', 'pad':5}, position=(0.5, 0.5),size = 13 )
plt.savefig("piechart_NaN.pdf")
plt.show()

#bar plot - cantidad de NaN encontrados por estacion 
plt.figure(figsize=(9,7))
plt.bar(estaciones, total_NaN_porEstacion, color = ["C0", "C1", "C2", "C3"])
plt.title("Cantidad de datos ausentes (NaN) para cuatro estaciones meteorologicas en CABA. \n" + " (años 2009 - 2020)", bbox={'facecolor':'0.8', 'pad':5}, position=(0.5, 0.5),size = 13 )
plt.savefig("barplot_NaN.pdf")
plt.show()

#%%
##### MAPAS

#Mapas semanales con promedios de NO2
for i in range(len(inicio_semana)):
    mapa_promedios = folium.Map(location = [latitud_media, longitud_media], zoom_start = 14)
    year = inicio_semana[i].year
    if promedio_semanal_la_boca[i] != 0:    #si hay dato de promedio agrego circulo de tamaño proporcional
        folium.CircleMarker(
            location=[latitudes[0], longitudes[0]],                         #marker La Boca
            popup= (folium.Popup(html= nombres_estaciones_amb[0] + "<br>-Fecha inicio semana: "+\
            inicio_semana[i].strftime('%Y %b %d')+ "<br>-Promedio semanal contaminacion NO2: "\
                 + str(promedio_semanal_la_boca[i]), max_width = 200)),
            radius = generador_radio(promedio_semanal_la_boca[i], year, years, prom_anuales_generales),
            color = "green",
            fill = True,
            fill_color = "green",
            fill_opacity = 0.5,
            tooltip = nombres_estaciones_amb[0],
        ).add_to(mapa_promedios)
    else:
        folium.Marker(                                                      #marker sin datos
        location=[latitudes[0], longitudes[0]],
        popup= 'N/A',
        icon=folium.Icon(color = "green", icon = "remove-sign"),
        tooltip = nombres_estaciones_amb[0],
    ).add_to(mapa_promedios)

    if promedio_semanal_centenario[i] != 0:
        folium.CircleMarker(                                                      #marker Centenario
            location=[latitudes[1], longitudes[1]],
            popup= (folium.Popup(html= nombres_estaciones_amb[1] + "<br>-Fecha inicio semana: "+\
            inicio_semana[i].strftime('%Y %b %d')+ "<br>-Promedio semanal contaminacion NO2: "\
                 + str(promedio_semanal_centenario[i]), max_width = 200)),
            radius = generador_radio(promedio_semanal_centenario[i], year, years, prom_anuales_generales),
            color = "blue",
            fill = True,
            fill_color = "blue",
            fill_opacity = 0.5,
            tooltip = nombres_estaciones_amb[1]
        ).add_to(mapa_promedios)
    else:
        folium.Marker(                                                      #marker sin datos
        location=[latitudes[1], longitudes[1]],
        popup= 'N/A',
        icon=folium.Icon(color = "blue", icon = "remove-sign"),
        tooltip = nombres_estaciones_amb[1],
    ).add_to(mapa_promedios)

    if promedio_semanal_cordoba[i] != 0:
        folium.CircleMarker(                                                      #marker Cordoba
            location=[latitudes[2], longitudes[2]],
            popup= (folium.Popup(html= nombres_estaciones_amb[2] + "<br>-Fecha inicio semana: "+\
            inicio_semana[i].strftime('%Y %b %d')+ "<br>-Promedio semanal contaminacion NO2: "\
                 + str(promedio_semanal_cordoba[i]), max_width = 200)),
            radius = generador_radio(promedio_semanal_cordoba[i], year, years, prom_anuales_generales),
            color = "orange",
            fill = True,
            fill_color = "orange",
            fill_opacity = 0.5,
            tooltip = nombres_estaciones_amb[2],
        ).add_to(mapa_promedios)
    else:
        folium.Marker(                                                      #marker Cordoba sin datos
        location=[latitudes[2], longitudes[2]],
        popup= 'N/A',
        icon=folium.Icon(color = "orange", icon = "remove-sign"),
        tooltip = nombres_estaciones_amb[2],
    ).add_to(mapa_promedios)

    if promedio_semanal_palermo[i] != 0:
        folium.CircleMarker(                                                      #marker Palermo
            location=[latitudes[3], longitudes[3]],
            popup= (folium.Popup(html= nombres_estaciones_amb[3] + "<br>-Fecha inicio semana: "+\
            inicio_semana[i].strftime('%Y %b %d')+ "<br>-Promedio semanal contaminacion NO2: "\
                 + str(promedio_semanal_palermo[i]), max_width = 200)),
            radius = generador_radio(promedio_semanal_palermo[i], year, years, prom_anuales_generales),
            color = "red",
            fill = True,
            fill_color = "red",
            fill_opacity = 0.5,
            tooltip = nombres_estaciones_amb[3],
        ).add_to(mapa_promedios)
    else:
        folium.Marker(                                                      #marker palermo sin datos
        location=[latitudes[3], longitudes[3]],
        popup= 'N/A',
        icon=folium.Icon(color = "red", icon = "remove-sign"),
        tooltip = nombres_estaciones_amb[3],
    ).add_to(mapa_promedios)
    mapa_promedios.save(f"mapa_promedios{inicio_semana[i].strftime('%Y %b %d')}.html")

#%%
#####Mapa con informacion de las estaciones

mapa = folium.Map(location = [latitud_media, longitud_media], zoom_start = 14)
pop_up = []
for i in range(4):                                                  #popup para c/estacion
    pop_up.append(folium.Popup(html= nombres_estaciones_amb[i] + "<br>-Dirección: "+\
     direccion[i]+ "<br>-Información adicional: " + info[i], max_width = 400))
folium.Marker(
    location=[latitudes[0], longitudes[0]],                         #marker La Boca
    popup= pop_up[0],
    icon=folium.Icon(color = "green"),
    tooltip = nombres_estaciones_amb[0],
).add_to(mapa)

folium.Marker(                                                      #marker Centenario
    location=[latitudes[1], longitudes[1]],
    popup= pop_up[1],
    icon=folium.Icon(color = "lightblue"),
    tooltip = nombres_estaciones_amb[1]
).add_to(mapa)

folium.Marker(                                                      #marker Cordoba
    location=[latitudes[2], longitudes[2]],
    popup= pop_up[2],
    icon=folium.Icon(color = "orange"),
    tooltip = nombres_estaciones_amb[2],
).add_to(mapa)

folium.Marker(                                                      #marker Palermo
    location=[latitudes[3], longitudes[3]],
    popup= pop_up[3],
    icon=folium.Icon(color = "red"),
    tooltip = nombres_estaciones_amb[3],
).add_to(mapa)

mapa.save("mapa_sin_promedios.html")
# %%